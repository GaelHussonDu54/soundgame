import 'package:flutter/material.dart';
import 'package:jeudessons/Blocs/lupixjeudessons/GameConstruction/Game.dart';
import 'package:jeudessons/Ressources/lupixjeudessons/GradientColor.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/BackgroundChoice.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

compteurLetterRound(BuildContext context, int _index, double value) {
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width / 1.4,
          child: LinearPercentIndicator(
              linearGradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors:
                    BackgroundChoice.colorBlue ? orangeGradient : blueGradient,
              ),
              percent: value,
              lineHeight: 25,
              backgroundColor: Colors.green[100],
              width: MediaQuery.of(context).size.width / 1.4,
              center: Text(
                "${_index + 1} /${SoundGame.numberRound}",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.height / 40,
                    fontWeight: FontWeight.bold),
              )),
        ),
      ],
    ),
  );
}

construcRotate(item, int i) {
  return RotationTransition(
    turns: item['animation'],
    child: Text(
      item['lettre'],
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: item['taillelettre'].toDouble(),
        color: item['color$i'],
      ),
    ),
  );
}
