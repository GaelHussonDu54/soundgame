import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Helper/lupixjeudessons/AnimatedImage.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ChoiceRound.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ColorChoice.dart';
import 'package:jeudessons/Helper/lupixjeudessons/FunctionUsefull.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ResponsiveClass.dart';
import 'package:jeudessons/Ressources/lupixjeudessons/Image.dart';
import 'package:jeudessons/Ressources/lupixjeudessons/Sound.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/NumberPicture.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/NumberRound.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/WordsChoice/Category.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/WordsChoice/WordsChoice.dart';
import 'ConstructionRound.dart';

class SoundGame extends StatefulWidget {
  const SoundGame({Key? key}) : super(key: key);
  static int numberRound = NumberRound.choiceRound;

  @override
  _SoundGame createState() => _SoundGame();
}

class _SoundGame extends State<SoundGame> {
  int _index = 0;
  late List randomListRound = [];
  late List listchoosen; // Exemple: animal : Il y a tous les animaux
  late List pictureList;
  int randomNumber = 0;
  late String animalFind;
  late List listRound;
  String category = '';
  late String findSound;

  _SoundGame() {
    pictureList = listCreation(NumberPicture.choicePicture);
    listRound = listCreation(NumberRound.choiceRound);
    construction();
  }

  construction() {
    if (Words.choiceCat) {
      remplissageCat(CategoryChoice.listfinal);

      category = CategoryChoice.listfinal[_index];
      listchoosen = getTheList(category);
    } else {
      listchoosen = listAllSound;
      print(listchoosen);
    }

    for (int i = 0; i < NumberPicture.choicePicture; i++) {
      randomNumber = randomNum(listchoosen.length);
      animalFind = listchoosen[randomNumber];
      randomListRound.add(animalFind);
      listchoosen.remove(animalFind); // on enleve le chat de notre liste
    }

    findSound =
        randomListRound.elementAt(randomNum(randomListRound.length));

    for (var item1 in pictureList) {
      item1['son'] = findSound;

      String find =
          randomListRound.elementAt(randomNum(randomListRound.length));
      randomListRound.remove(find);
      item1['image'] = display(find, 'jpg');
      item1['color'] = Color.fromARGB(0, 0, 0, 0);
      item1['rep'] = find;
      listchoosen.add(find);
    }

    print('Voici la liste des round : $listRound');
    print('Voici la liste des cat : ${CategoryChoice.listfinal}');

    print(findSound);
    Future.delayed(const Duration(seconds: 0, milliseconds: 300), () {
      listen(findSound);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stepper(
        physics: NeverScrollableScrollPhysics(),
        controlsBuilder: (BuildContext context,
            {VoidCallback? onStepContinue, VoidCallback? onStepCancel}) {
          return Row(
            children: <Widget>[
              Container(child: null),
              Container(
                child: null,
              ),
            ],
          );
        },
        type: StepperType.horizontal,
        currentStep: _index,
        steps: <Step>[
          for (var item in listRound)
            Step(
              title: const Text(''),
              content: Column(children: <Widget>[
                Row(children: [
                  SizedBox(
                    width: responsivSizeBox('width', 300, context),
                  ),
                  compteurLetterRound(
                      context, _index, (_index) / SoundGame.numberRound),
                  SizedBox(
                    width: responsivSizeBox('width', 300, context),
                  ),
                  IconButton(
                    icon: Icon(
                      IconData(60859, fontFamily: 'MaterialIcons'),
                      size: MediaQuery.of(context).size.width / 12,
                    ),
                    onPressed: () {
                      listen(findSound);
                    },
                  ),
                ]),
                NumberPicture.choicePicture < 3
                    ? SizedBox(
                        height: responsivSizeBox('height', 20, context),
                      )
                    : SizedBox(),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height/1.1, //286
                    width: MediaQuery.of(context).size.width,

                    child: Wrap(
                      alignment: WrapAlignment.center,
                      spacing: 50,
                      children: <Widget>[
                        for (var item in pictureList)
                          GestureDetector(
                              child: Container(
                                  padding: const EdgeInsets.all(10.0),
                                  width:  NumberPicture.choicePicture < 3?
                                  MediaQuery.of(context).size.width / 2.5:
                                  MediaQuery.of(context).size.width / 2.9,
                                  height:  NumberPicture.choicePicture < 3?
                                  MediaQuery.of(context).size.height / 3.3:
                                  MediaQuery.of(context).size.height / 5.2,
                                  child: contour(
                                      context, item['color'], item['image'])),
                              onTap: () {
                                setState(() {
                                  // item1['carte'] = item1['image'];
                                  if (item['rep'] == item['son']) {
                                    item['color'] = Colors.green;
                                    Future.delayed(
                                        const Duration(
                                            seconds: 0, milliseconds: 0), () {
                                      listen('clique');
                                    });
                                    fetchUserOrder(1,0,findSound);
                                    Future.delayed(
                                        const Duration(
                                            seconds: 2, milliseconds: 600), () {
                                      listen('super');
                                    });
                                    Future.delayed(
                                        const Duration(
                                            seconds: 5, milliseconds: 300), () {
                                      listen('applaudissement');
                                    });
                                    Future.delayed(const Duration(seconds: 7,milliseconds: 500),
                                        () {
                                      setState(() {
                                        _index += 1;
                                        if (_index < NumberRound.choiceRound) {
                                          construction();
                                        }
                                      });
                                    });
                                  } else {
                                    item['color'] = Colors.red;
                                    listen('oups');
                                  }
                                });
                              }),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
          Step(
            //Victoire
            state: StepState.complete,
            title: const Text('Fin !'),
            content: LogoApp(),
          ),
        ]);
  }
}
