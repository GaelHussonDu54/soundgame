import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jeudessons/Ui/UIGame/topTab.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/BackgroundChoice.dart';

import 'Game.dart';

class MyAppStarteur extends StatelessWidget {
  const MyAppStarteur({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: GameStarteur(),
      ),
    );
  }
}

class GameStarteur extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(children: <Widget>[
          YeuxScreen(),
      SizedBox(height: MediaQuery.of(context).size.height / 4.16),

      Container(
        height: MediaQuery.of(context).size.height/ 1.84 ,
        child: Stack(children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      BackgroundChoice.colorBack1,
                      BackgroundChoice.colorBack2,
                    ],
                  ),
                ),
              ),
              Container(
                child: SoundGame(),
              ),
            ]),
          ),

      ]),
    );
  }
}
