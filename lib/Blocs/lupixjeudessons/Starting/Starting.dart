import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Blocs/lupixjeudessons/GameConstruction/GameStarteur.dart';
import 'package:jeudessons/Helper/lupixjeudessons/GameStarted.dart';
import 'package:jeudessons/Ressources/lupixjeudessons/GradientColor.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/AppBar.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/Validation.dart';

import 'CustomizeGame.dart';

class Starting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePageLupixABC(),
    );
  }
}

class HomePageLupixABC extends StatefulWidget {
  @override
  _HomePageLupixABCState createState() => _HomePageLupixABCState();
}

class _HomePageLupixABCState extends State<HomePageLupixABC> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      body: CustomizeGame(),
    );
  }
}

/// This is the private State class that goes with MyStatefulWidget.

class Validation extends StatefulWidget {
  @override
  _ValidationState createState() => _ValidationState();
}

class _ValidationState extends State<Validation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        child: ValidationUi(
            text: 'Valider',
            size: MediaQuery.of(context).size.height / 30,
            color: orangeGradient,
            colorText: Colors.white),
        onTap: () => {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return MyAppStarteur();
          })),
        },
        onLongPress: () => {
          LoaderGameStarted.appLoader.showLoader(),
        },
      ),
    );
  }
}
