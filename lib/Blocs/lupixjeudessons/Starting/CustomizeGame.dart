import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/BackgroundChoice.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/NumberRound.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/NumberPicture.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/WordsChoice/Category.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/WordsChoice/WordsChoice.dart';
import 'Starting.dart';

class CustomizeGame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Stack(children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.lightBlueAccent,
            child: Column(
              children: <Widget>[
                SizedBox(height: MediaQuery.of(context).size.height / 24),
                Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[
                  BackgroundChoice(),
                  SizedBox(width: MediaQuery.of(context).size.width / 40),
                  NumberPicture(),

                ]),
                SizedBox(height: MediaQuery.of(context).size.height / 24),

                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Words(),
                      SizedBox(width: MediaQuery.of(context).size.width / 40),

                      NumberRound(),

                    ]),
                SizedBox(height: MediaQuery.of(context).size.height / 15),
                Validation(),
                SizedBox(height: MediaQuery.of(context).size.height / 19),
              ],
            ),
          ),
          OverlayView(),
        ]),
      ),
    );
  }
}
