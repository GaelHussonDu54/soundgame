import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Blocs/lupixjeudessons/GameConstruction/GameStarteur.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ColorChoice.dart';
import 'package:jeudessons/Helper/lupixjeudessons/CreationContener.dart';
import 'package:jeudessons/Helper/lupixjeudessons/GameStarted.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ResponsiveClass.dart';
import 'package:jeudessons/Ressources/lupixjeudessons/GradientColor.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/BackgroundChoice.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/NumberPicture.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/NumberRound.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/Validation.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/WordsChoice/Category.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/WordsChoice/WordsChoice.dart';
modeSelected(BuildContext context) {
  return Column(children: <Widget>[
    Wrap(alignment: WrapAlignment.center, spacing: 45, children: <Widget>[
      Container(
        child: Column(children: <Widget>[
          Text('Fond choisi',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: MediaQuery.of(context).size.height / 40,
              ),
              textAlign: TextAlign.center),
          SizedBox(
            height: responsivSizeBox('height', 10, context),
          ),
          Container(
              width: IconeChoice('width', context, 'category'),
              height: IconeChoice('height', context, 'category'),
              decoration: boxDeco(Color.fromARGB(0, 0, 0, 0), BackgroundChoice.colorBack1, BackgroundChoice.colorBack2)),
        ]),
      ),
      Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(NumberPicture.titleChoose,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: MediaQuery.of(context).size.height / 40,
                  ),
                  textAlign: TextAlign.center),
              SizedBox(
                height: responsivSizeBox('height', 10, context),
              ),
              myContainer(context, Colors.green, '3'),
            ]),
      ),
    ]),
    SizedBox(
      height: responsivSizeBox('height', 25, context),
    ),
    Wrap(alignment: WrapAlignment.center, spacing: 45, children: <Widget>[
      Container(
        child: Column(children: <Widget>[

          SizedBox(
            height: responsivSizeBox('height', 10, context),
          ),
          roundContainer(context, Colors.green, '$NumberRound.choiceRound')
        ]),
      ),
      Container(
        child: Column(children: <Widget>[
          Text(Words.titleWordsChoose,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: MediaQuery.of(context).size.height / 40,
              ),
              textAlign: TextAlign.center),
          SizedBox(
            height: responsivSizeBox('height', 10, context),
          ),
          Words.choiceCat
              ? contour(context, Color.fromARGB(0, 0, 0, 0),
                  CategoryChoice.catChoose)
              : boxAl(context, Color.fromARGB(0, 0, 0, 0)),
        ]),
      ),
    ])
  ]);
}


class Retry extends StatefulWidget {
  @override
  _Retry createState() => _Retry();
}

class _Retry extends State<Retry> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        child: ValidationUi(text: 'Relancer la partie',

            size: MediaQuery.of(context).size.height / 35,
        color:greenGradient, colorText: Colors.black),
        onTap: () => {LoaderGameStarted.appLoader.hideLoader(),
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return MyAppStarteur();
          })),
        },
      ),
    );
  }
}

class StopGame extends StatefulWidget {
  @override
  _StopGame createState() => _StopGame();
}

class _StopGame extends State<StopGame> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        child: ValidationUi(
            text: 'Arrêter la partie',
            size: MediaQuery.of(context).size.height / 35,
        color:blueGradient,colorText: Colors.white),
        onTap: () => {LoaderGameStarted.appLoader.hideLoader()},
      ),
    );
  }
}


