import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Ui/UIGame/topTab.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: MyHome(),
    );
  }
}


class MyHome extends StatelessWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: InkWell(
              child: Container(
                margin: const EdgeInsets.only(bottom: 350.0),
                child: YeuxScreen(),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 40.0),

            alignment: Alignment.topLeft,

            child: IconButton(

              icon: Icon(Icons.arrow_back, size: 38.0),
              tooltip: 'Connexion au bluetooth', onPressed: () {

            },
            ),
          ),
          Container(

            height: 100,
            margin: const EdgeInsets.only(bottom: 130.0),
            child: ElevatedButton(

              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => QRViewExample(),
                ));
              },
              child: Text('Lancer le scan',),
            ),
          ),
        ],),
    );
  }
}

class QRViewExample extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _QRViewExampleState();
}

class _QRViewExampleState extends State<QRViewExample> {
  Barcode? result;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: InkWell(
              child: Container(
                margin: const EdgeInsets.only(bottom: 10.0),
                child: YeuxScreen(),
              ),
            ),
          ),
          Expanded(flex: 2, child: _buildQrView(context)),

          Container(
            height: 50.0,
            child: FittedBox(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  if (result != null)
                    Text(
                        'Type de barcode: ${describeEnum(result!.format)}   Donnée reçu: ${result!.code}')
                  else
                    Text('Scanner un qr code', style: TextStyle(fontSize: 1),
                    ),

                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
        MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
      this.controller?.flipCamera();

    });
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        result = scanData;
      });

    });
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('no Permission')),
      );
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

