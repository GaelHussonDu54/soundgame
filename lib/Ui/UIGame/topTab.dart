
import 'package:flutter/material.dart';

class YeuxScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: MediaQuery.of(context).size.height / 23),
        Row(
          children: <Widget>[
            SizedBox(width: MediaQuery.of(context).size.width / 6.2),
            Container(
              // Oeil gauche
              width: MediaQuery.of(context).size.width / 4,
              height: MediaQuery.of(context).size.height / 6,
              decoration: BoxDecoration(
                image: new DecorationImage(
                  fit: BoxFit.fill,
                  image: new AssetImage('assets/image/rouge-004.png'),
                ),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 5,
            ),
            Container(
              // Oeil droit
              width: MediaQuery.of(context).size.width / 4, //2.5,
              height: MediaQuery.of(context).size.height / 6, // 4,
              decoration: BoxDecoration(
                image: new DecorationImage(
                  fit: BoxFit.fill,
                  image: new AssetImage('assets/image/rouge-004.png'),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}