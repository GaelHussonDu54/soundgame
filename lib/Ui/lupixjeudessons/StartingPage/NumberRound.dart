import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Helper/lupixjeudessons/CreationContener.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ResponsiveClass.dart';


class NumberRound extends StatefulWidget {
  static int choiceRound = 6;


  @override
  _NumberRoundState createState() => _NumberRoundState();
}

class _NumberRoundState extends State<NumberRound> {
  Color color1 = Colors.greenAccent;
  Color color2 = Colors.greenAccent;
  Color color3 = Colors.greenAccent;
  Color color4 = Colors.greenAccent;


  //List numberRound = [30.toDouble(), 40.toDouble(), 50.toDouble()];
  List numberRound = [4, 6, 8, 10];

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.48,
      height: MediaQuery.of(context).size.height * 0.32,
      color: Color.fromARGB(50, 255, 0, 0),
      child: Column(children: <Widget>[
        SizedBox(height: responsivSizeBox('height', 15, context)),
        Text('Choix du nombre de manches',
            style: TextStyle(
              color: Colors.black,
              fontSize: MediaQuery.of(context).size.height / 36,
              fontWeight: FontWeight.w400,
            )),
        SizedBox(height: responsivSizeBox('height', 10, context)),
        Container(
          width: MediaQuery.of(context).size.width * 0.45,
          height: MediaQuery.of(context).size.height * 0.18,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Row(
            children: <Widget>[
              SizedBox(width: responsivSizeBox('width', 30, context)),
              GestureDetector(
                  child: roundContainer(context, color1, '4'),
                  onTap: () => {
                    NumberRound.choiceRound =
                        numberRound[0],

                    setState(() {
                      color1 = Color(0xFF1B5E20); // green foncé
                      color2 = Colors.greenAccent;
                      color3 = Colors.greenAccent;
                      color4 = Colors.greenAccent;

                    }),
                  }),
              SizedBox(width: responsivSizeBox('width', 30, context)),
              GestureDetector(
                  child: roundContainer(context, color2, '6'),
                  onTap: () => {
                    NumberRound.choiceRound =
                        numberRound[1],

                    setState(() {
                      color2 = Color(0xFF1B5E20); // green foncé
                      color1 = Colors.greenAccent;
                      color3 = Colors.greenAccent;
                      color4 = Colors.greenAccent;
                    }),
                  }),
              SizedBox(width: responsivSizeBox('width', 30, context)),
              GestureDetector(
                  child: roundContainer(context, color3, '8'),
                  onTap: () => {
                    NumberRound.choiceRound =
                        numberRound[2],
                    setState(() {
                      color3 = Color(0xFF1B5E20); // green foncé
                      color2 = Colors.greenAccent;
                      color1 = Colors.greenAccent;
                      color4 = Colors.greenAccent;

                    }),
                  }),
              SizedBox(width: responsivSizeBox('width', 30, context)),
              GestureDetector(
                  child: roundContainer(context, color4, '10'),
                  onTap: () => {
                    NumberRound.choiceRound =
                        numberRound[3],
                    setState(() {
                      color4 = Color(0xFF1B5E20); // green foncé
                      color2 = Colors.greenAccent;
                      color1 = Colors.greenAccent;
                      color3 = Colors.greenAccent;

                    }),
                  }),
            ],
          ),
        ),
      ],
      ), //gris
    );
  }
}
