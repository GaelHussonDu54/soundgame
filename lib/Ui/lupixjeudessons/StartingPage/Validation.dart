
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class ValidationUi extends StatelessWidget{
  const ValidationUi({ required this.text, required this.size, required this.color, required this.colorText}) ;
  final  String text;
  final double size;
  final List color;
  final Color colorText;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.20,
        height: MediaQuery.of(context).size.height * 0.07,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(colors:[
            color.elementAt(0),
            color.elementAt(1),
            color.elementAt(2),

          ]),
        ),
        child: Center(
          child: Text(text,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: colorText,
              fontWeight: FontWeight.bold,
              fontSize: size,
            ),
          ),
        ),
      ),
    );
  }
}

