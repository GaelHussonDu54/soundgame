import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  Size get preferredSize => new Size.fromHeight(50);
  static const IconData info = IconData(0xe33c, fontFamily: 'MaterialIcons');
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.indigo,
      title: Text(
        'Le monde des sons 1: Qui suis-je ?',
        style: TextStyle(
          color: Colors.white,
          fontSize: 22,
          fontWeight: FontWeight.w800,
        ),
      ),
      actions: <Widget>[
        // action button
        IconButton(
          icon: Icon(info),
          onPressed: () {
          },
        )
      ],
    );
  }
}
