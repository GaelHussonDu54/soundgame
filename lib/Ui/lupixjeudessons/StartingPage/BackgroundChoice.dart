import 'package:flutter/material.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ColorChoice.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ResponsiveClass.dart';

class BackgroundChoice extends StatefulWidget {
  static Color colorBack1 = Colors.blue;
  static Color colorBack2 = Colors.lightBlueAccent;
  static bool colorBlue = true;

  @override
  _BackgroundChoiceState createState() => _BackgroundChoiceState();
}

class _BackgroundChoiceState extends State<BackgroundChoice> {
  Color blueSelected = Color.fromARGB(0, 0, 0, 0);
  Color pinkSelected = Color.fromARGB(0, 0, 0, 0);
  Color blueBack = Colors.blue;
  Color pinkBack = Colors.pink;
  Color blue2Back = Colors.lightBlueAccent;
  Color pink2Back = Colors.pinkAccent;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: containerChoice('width', context, 'fond'),
      height: containerChoice('height', context, 'fond'),
      color: Color.fromARGB(50, 255, 0, 0), //gris
      child: Column(
        children: <Widget>[
          SizedBox(height: responsivSizeBox('height', 15, context)),
          Text(
            'Choix du fond',
            style: TextStyle(
              color: Colors.black,
              fontSize: MediaQuery.of(context).size.height / 29,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: responsivSizeBox('height', 25, context)),
          Wrap(
            spacing: responsivSizeBox('width', 20, context),
            alignment: WrapAlignment.center,
            children: <Widget>[
              GestureDetector(
                  child: backChoice(context, blueSelected, blueBack, blue2Back),
                  onTap: () => {
                        BackgroundChoice.colorBack1 = blueBack,
                        BackgroundChoice.colorBack2 = blue2Back,
                        BackgroundChoice.colorBlue = true,
                        setState(() {
                          blueSelected = Colors.green;
                          pinkSelected = Color.fromARGB(0, 0, 0, 0);
                        }),
                      }),
              GestureDetector(
                  child: backChoice(context, pinkSelected, pinkBack, pink2Back),
                  onTap: () => {
                        print('true'),
                        BackgroundChoice.colorBack1 = pinkBack,
                        BackgroundChoice.colorBack2 = pink2Back,
                        BackgroundChoice.colorBlue = false,
                        setState(() {
                          blueSelected = Color.fromARGB(0, 0, 0, 0);
                          pinkSelected = Colors.green;

                          // selectionColor();
                        }),
                      }),
            ],
          )
        ],
      ),
    );
  }
}
