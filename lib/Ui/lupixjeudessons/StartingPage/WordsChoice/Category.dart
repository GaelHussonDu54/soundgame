import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ColorChoice.dart';
import 'package:jeudessons/Helper/lupixjeudessons/FunctionUsefull.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ResponsiveClass.dart';
import 'package:jeudessons/Ressources/lupixjeudessons/GradientColor.dart';
import 'package:jeudessons/Ressources/lupixjeudessons/Image.dart';
import 'package:jeudessons/Ressources/lupixjeudessons/Sound.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/NumberRound.dart';

import '../Validation.dart';
import 'WordsChoice.dart';

class Loader {
  static final Loader appLoader = Loader();
  ValueNotifier<bool> loaderShowingNotifier = ValueNotifier(false);
  ValueNotifier<String> loaderTextNotifier = ValueNotifier('error message');

  void showLoader() {
    loaderShowingNotifier.value = true;
  }

  void hideLoader() {
    loaderShowingNotifier.value = false;
  }

  void setText({required String errorMessage}) {
    loaderTextNotifier.value = errorMessage;
  }

  void setImage() {
    // same as that of setText //
  }
}

class OverlayView extends StatelessWidget {
  // const OverlayView({Key key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: Loader.appLoader.loaderShowingNotifier,
      builder: (context, value, child) {
        if (value) {
          return yourOverLayWidget(context);
        } else {
          return Container();
        }
      },
    );
  }

  Container yourOverLayWidget(BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.5),
      child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Center(
          child: Column(children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.8,
              child: Card(
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 3.57,
                          child: Card(
                            margin: EdgeInsets.all(0),
                            color: Colors.green[100],
                            shadowColor: Colors.blueGrey,
                            elevation: 10,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Icon(Icons.album,
                                    color: Colors.cyan,
                                    size:
                                        MediaQuery.of(context).size.width / 27),
                                Text(
                                  "Choisissez la catégorie ",
                                  style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              60),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                        IconButton(
                          icon: Icon(Icons.close_outlined),
                          onPressed: () {
                            Loader.appLoader.hideLoader();
                          },
                        ),
                      ],
                    ),
                    SizedBox(
                      height: responsivSizeBox('height', 15, context),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.7,
                        height: MediaQuery.of(context).size.height * 0.55,
                        child: CategoryChoice()),
                    SizedBox(
                      height: responsivSizeBox('height', 15, context),
                    ),
                    ValidationCat(),
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}

class CategoryChoice extends StatefulWidget {
  static List list1 = [
    {
      'number': 0,
      'img': 'animals',
      'titre': 'Animaux',
      'color': Color.fromARGB(0, 0, 0, 0)
    },
    {
      'number': 0,
      'img': 'nature',
      'titre': 'Nature',
      'color': Color.fromARGB(0, 0, 0, 0)
    },
    {
      'number': 0,
      'img': 'maison',
      'titre': 'Bruits quotidiens',
      'color': Color.fromARGB(0, 0, 0, 0)
    },
  ];
  static List list2 = [
    {
      'number': 0,
      'img': 'transport',
      'titre': 'Transports',
      'color': Color.fromARGB(0, 0, 0, 0)
    },
    {
      'number': 0,
      'img': 'instrument',
      'titre': 'Instruments',
      'color': Color.fromARGB(0, 0, 0, 0)
    },
  ];
  static late List listfinal = []; //taille 5
  static String catTitleChoose = '';
  static String catChoose = 'assets/transport.jpg';
  static var colorValidation = greyGradient;

  @override
  _CategoryChoiceState createState() => _CategoryChoiceState();
}

class _CategoryChoiceState extends State<CategoryChoice> {
  Color colorvide = Color.fromARGB(0, 0, 0, 0);
  int num = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.3,
        child: Column(
          children: <Widget>[
            Wrap(
              spacing: responsivSizeBox('width', 15, context),
              children: <Widget>[
                for (var item in CategoryChoice.list1)
                  GestureDetector(
                    child: Column(children: <Widget>[
                      Text(item['titre'],
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w300,
                            fontSize: MediaQuery.of(context).size.height / 37,
                          )),
                      SizedBox(height: responsivSizeBox('height', 10, context)),
                      contour(
                          context, item['color'], display(item['img'], 'jpg')),
                      SizedBox(height: responsivSizeBox('height', 10, context)),
                      /*    Text('${item['number']}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w300,
                            fontSize: MediaQuery.of(context).size.height / 37,
                          )),*/
                    ]),
                    onTap: () => {
                      if (CategoryChoice.listfinal.length <
                              NumberRound.choiceRound &&
                          item['color'] != Colors.green)
                        {
                          CategoryChoice.listfinal.add(item['titre']),
                          setState(() {
                            // item['number'] += 1;
                            //   num = (num + item['number']).toInt();
                            item['color'] = Colors.green;
                          }),
                        }
                      else if (item['color'] == Colors.green)
                        {
                          removeValue(CategoryChoice.listfinal, item['titre']),
                          setState(() {
                            item['color'] = colorvide;
                            item['number'] = 0;
                          }),
                        }
                      else
                        {
                          audioPlayer.open(Audio('assets/error.mp3')),
                        }
                    },
                    onLongPress: () => {
                      removeValue(CategoryChoice.listfinal, item['titre']),
                      print(CategoryChoice.listfinal),
                      setState(() {
                        item['color'] = colorvide;
                        item['number'] = 0;
                      }),
                    },
                  ),
              ],
            ),
            SizedBox(
              height: responsivSizeBox('height', 10, context),
            ),
            Wrap(
              spacing: responsivSizeBox('width', 15, context),
              children: <Widget>[
                for (var item in CategoryChoice.list2)
                  GestureDetector(
                      child: Column(children: <Widget>[
                        Text(item['titre'],
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w300,
                              fontSize: MediaQuery.of(context).size.height / 37,
                            )),
                        SizedBox(
                            height: responsivSizeBox('height', 10, context)),
                        contour(context, item['color'],
                            display(item['img'], 'jpg')),
                        SizedBox(
                            height: responsivSizeBox('height', 10, context)),
                       /* Text('${item['number']}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w300,
                              fontSize: MediaQuery.of(context).size.height / 37,
                            )),*/
                      ]),
                      onTap: () => {
                            if (CategoryChoice.listfinal.length <
                                    NumberRound.choiceRound &&
                                item['color'] != Colors.green)
                              {
                                CategoryChoice.listfinal.add(item['titre']),
                                setState(() {
                                  // item['number'] += 1;
                                  //   num = (num + item['number']).toInt();
                                  item['color'] = Colors.green;
                                }),
                              }
                            else if (CategoryChoice.listfinal.length <
                                    NumberRound.choiceRound &&
                                item['color'] == Colors.green)
                              {
                                removeValue(
                                    CategoryChoice.listfinal, item['titre']),
                                setState(() {
                                  item['color'] = colorvide;
                                  item['number'] = 0;
                                }),
                              }
                            else
                              {
                                audioPlayer.open(Audio('assets/error.mp3')),
                              }
                          },
                      onLongPress: () => {
                            removeValue(
                                CategoryChoice.listfinal, item['titre']),
                            print(CategoryChoice.listfinal),
                            setState(() {
                              item['color'] = colorvide;
                              item['number'] = 0;
                            }),
                          }),
              ],
            ),
          ],
        )

        //gris
        );
  }
}

class ValidationCat extends StatefulWidget {
  @override
  _CtValidationState createState() => _CtValidationState();
}

class _CtValidationState extends State<ValidationCat> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        child: ValidationUi(
            text: 'Valider',
            size: MediaQuery.of(context).size.height / 30,
            color: orangeGradient,
            colorText: Colors.white),
        onTap: () => {
          if (CategoryChoice.listfinal.length!=0 )
            {
              Loader.appLoader.hideLoader(),
              Words.titleWordsChoose = CategoryChoice.catTitleChoose,

            }
          else
            {
              listen('error'),
            }
        },
      ),
    );
  }
}
