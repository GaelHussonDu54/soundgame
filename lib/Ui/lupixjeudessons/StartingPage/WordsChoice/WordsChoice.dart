import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ColorChoice.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ResponsiveClass.dart';
import 'Category.dart';

class Words extends StatefulWidget {
  static bool choiceCat = false;
  static String titleWordsChoose='Aléatoire';

  @override
  _WordsState createState() => _WordsState();
}

class _WordsState extends State<Words> {
  Color color1 = Color.fromARGB(0, 0, 0, 0);
  Color color2 = Color.fromARGB(0, 0, 0, 0);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: containerChoice('width', context, 'word'),
      height: containerChoice('height', context, 'word'),
      color: Color.fromARGB(50, 255, 0, 0),
      child: Column(children: <Widget>[
        SizedBox(height: responsivSizeBox('height', 15, context)),
        // Spacer(),
        Text('Choix des sons',
            style: TextStyle(
              color: Colors.black,
              fontSize: MediaQuery.of(context).size.height / 29,
              fontWeight: FontWeight.w600,
            )),
        SizedBox(height: responsivSizeBox('height', 12, context)),
        Wrap(
          // Row
          spacing: responsivSizeBox('width', 20, context),
          alignment: WrapAlignment.center,
          children: <Widget>[
            GestureDetector(
                child: Column(children: <Widget>[
                  Text('Choix des 5 sons',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w300,
                        fontSize: MediaQuery.of(context).size.height / 39,
                      )),
                  SizedBox(height: responsivSizeBox('height', 10, context)),
                  boxAl(context, color1),
                ]),
                onTap: () => {
                      Words.choiceCat = false,
                      setState(() {
                        color2 = Color.fromARGB(0, 0, 0, 0);
                        color1 = Colors.green;
                      }),
                    }),
            GestureDetector(
              child: Column(children: <Widget>[
                Text('Choix de la catégorie',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                      fontSize: MediaQuery.of(context).size.height / 37,
                    )),
                SizedBox(height: responsivSizeBox('height', 10, context)),
                boxCat(context, color2),
              ]),
              onTap: () => {
                Words.choiceCat = true,
                Loader.appLoader.showLoader(),
                setState(() {
                  color1 = Color.fromARGB(0, 0, 0, 0);
                  color2 = Colors.green;
                }),
                /* Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return WordsCategory();
                })),*/
              },
            ),
          ],
        ),
      ]),
      //gris
    );
  }
}
