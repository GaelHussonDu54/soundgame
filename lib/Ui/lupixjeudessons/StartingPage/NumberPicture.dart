import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Helper/lupixjeudessons/CreationContener.dart';
import 'package:jeudessons/Helper/lupixjeudessons/ResponsiveClass.dart';


class NumberPicture extends StatefulWidget {
  static int choicePicture = 2;

  static String titleChoose='2 image';
  @override
  _NumberPictureState createState() => _NumberPictureState();
}

class _NumberPictureState extends State<NumberPicture> {
  Color color1 = Colors.greenAccent;
  Color color2 = Colors.greenAccent;
  Color color3 = Colors.greenAccent;

  //List numberPicture = [30.toDouble(), 40.toDouble(), 50.toDouble()];
  List numberPicture = [2, 3, 4];

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.48,
      height: MediaQuery.of(context).size.height * 0.32,
      color: Color.fromARGB(50, 255, 0, 0),
      child: Column(children: <Widget>[
            SizedBox(height: responsivSizeBox('height', 15, context)),
            Text('Choix du nombre d\u0027images affichées',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: MediaQuery.of(context).size.height / 36,
                  fontWeight: FontWeight.w400,
                )),
            SizedBox(height: responsivSizeBox('height', 10, context)),
            Container(
              width: MediaQuery.of(context).size.width * 0.45,
              height: MediaQuery.of(context).size.height * 0.18,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Row(
                children: <Widget>[
                  SizedBox(width: responsivSizeBox('width', 50, context)),
                  GestureDetector(
                      child: myContainer(context, color1, '2'),
                      onTap: () => {
                        NumberPicture.choicePicture =
                            numberPicture[0],
                        setState(() {
                          color1 = Color(0xFF1B5E20); // green foncé
                          color2 = Colors.greenAccent;
                          color3 = Colors.greenAccent;
                        }),
                      }),
                  SizedBox(width: responsivSizeBox('width', 30, context)),
                  GestureDetector(
                      child: myContainer(context, color2, '3'),
                      onTap: () => {
                        NumberPicture.choicePicture =
                            numberPicture[1],
                        setState(() {
                          color2 = Color(0xFF1B5E20); // green foncé
                          color1 = Colors.greenAccent;
                          color3 = Colors.greenAccent;
                        }),
                      }),
                  SizedBox(width: responsivSizeBox('width', 30, context)),
                  GestureDetector(
                      child: myContainer(context, color3, '4'),
                      onTap: () => {
                        NumberPicture.choicePicture =
                            numberPicture[2],
                        setState(() {
                          color3 = Color(0xFF1B5E20); // green foncé
                          color2 = Colors.greenAccent;
                          color1 = Colors.greenAccent;
                        }),
                      }),
                ],
              ),
            ),
        ],
      ), //gris
    );
  }
}
