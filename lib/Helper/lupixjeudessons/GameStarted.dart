import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Blocs/lupixjeudessons/Starting/GameStarted.dart';
import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/BackgroundChoice.dart';

import 'ResponsiveClass.dart';
class LoaderGameStarted {
  static final LoaderGameStarted appLoader = LoaderGameStarted();
  ValueNotifier<bool> loaderShowingNotifier = ValueNotifier(false);
  ValueNotifier<String> loaderTextNotifier = ValueNotifier('error message');

  void showLoader() {
    loaderShowingNotifier.value = true;
  }

  void hideLoader() {
    loaderShowingNotifier.value = false;
  }

  void setText({required String errorMessage}) {
    loaderTextNotifier.value = errorMessage;
  }

  void setImage() {
    // same as that of setText //
  }
}

class OverlayGameStarted extends StatelessWidget {
  // const OverlayView({Key key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: LoaderGameStarted.appLoader.loaderShowingNotifier,
      builder: (context, value, child) {
        if (value) {
          return yourOverLay(context);
        } else {
          return Container();
        }
      },
    );
  }

  Container yourOverLay(BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.5),
      child: Padding(
        padding: const EdgeInsets.all(36.0),
        child: Center(
          child: Column(children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.80,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      BackgroundChoice.colorBack1,
                      BackgroundChoice.colorBack2,
                    ],
                  ),

                ),
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 3.3,
                          child: Card(
                            margin: EdgeInsets.all(0),
                            color: Colors.green[100],
                            shadowColor: Colors.blueGrey,
                            elevation: 10,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Icon(Icons.album,
                                    color: Colors.cyan,
                                    size:
                                        MediaQuery.of(context).size.width / 27),
                                Text(
                                  "Partie en cours... ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              60),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,

                          ),
                          child: IconButton(
                            icon: Icon(Icons.close_outlined),
                            onPressed: () {
                              LoaderGameStarted.appLoader.hideLoader();
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: responsivSizeBox('height', 15, context),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.7,
                      height: MediaQuery.of(context).size.height * 0.57,
                      //  color: Color.fromARGB(250, 235, 235, 239),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [
                            Colors.blueAccent,
                            Colors.greenAccent,
                          ],
                        ),
                      ),
                      child: Column(children: <Widget>[
                        SizedBox(
                          height: responsivSizeBox('height', 10, context),
                        ),
                        Text('Votre Sélection : ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize:
                                    MediaQuery.of(context).size.height / 30)),
                        SizedBox(
                            height: responsivSizeBox('height', 15, context)),
                        Container(
                          child: modeSelected(context),
                        )
                      ]),
                    ),
                    SizedBox(height: responsivSizeBox('height', 20, context)),
                    Wrap(
                        alignment: WrapAlignment.center,
                        direction: Axis.horizontal,
                        spacing: responsivSizeBox('width', 25, context),
                        children: [
                          Retry(),
                          StopGame(),
                        ])
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
