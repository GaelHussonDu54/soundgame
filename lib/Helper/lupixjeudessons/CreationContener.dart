
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

myContainer(BuildContext context, Color color1, String s) {
  return Container(
    width: MediaQuery.of(context).size.width * 0.10,
    height: MediaQuery.of(context).size.height * 0.15,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: color1,
    ),
    child: Center(
      child: Text(
        s,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.cyan,
          fontWeight: FontWeight.bold,
          fontSize: MediaQuery.of(context).size.height / 13,
        ),
      ),
    ),
  );
}


roundContainer(BuildContext context, Color color1, String s) {
  return Container(
    width: MediaQuery.of(context).size.width * 0.08,
    height: MediaQuery.of(context).size.height * 0.12,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: color1,
    ),
    child: Center(
      child: Text(
        s,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.cyan,
          fontWeight: FontWeight.bold,
          fontSize: MediaQuery.of(context).size.height / 13,
        ),
      ),
    ),
  );
}