import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ResponsiveClass.dart';

Container contour(BuildContext context, Color color, String s) {
  return Container(
    width: IconeChoice('width', context, 'category'),
    height: IconeChoice('height', context, 'category'),
    decoration: BoxDecoration(
      image: new DecorationImage(
        fit: BoxFit.fill,
        image: new AssetImage(
          s,
        ),
      ),
      boxShadow: ([
        BoxShadow(
          offset: const Offset(1.0, 1.0),
          blurRadius: 10.0,
          spreadRadius: 1.2,
        ),
      ]),
      border: Border(
        left: BorderSide(
          color: color,
          width: 8,
        ),
        right: BorderSide(
          color: color,
          width: 8,
        ),
        top: BorderSide(
          color: color,
          width: 8,
        ),
        bottom: BorderSide(
          color: color,
          width: 8,
        ),
      ),
      borderRadius: BorderRadius.circular(10),
      color: Colors.white,
    ),
  );
}

Container boxCat(BuildContext context, Color color) {
  return Container(
      width: IconeChoice('width', context, 'word'),
      height: IconeChoice('height', context, 'word'),
      child: Column(children: [
        SizedBox(
          height: responsivSizeBox('height', 6, context),
        ),
        Wrap(
            spacing: responsivSizeBox('width', 20, context),
            children: <Widget>[
              card(context, 'Transport', 10, Color(0xff30cfd0),
                  Color(0xff330867), Color(0xff330867)),
            ]),
        SizedBox(
          height: responsivSizeBox('height', 20, context),
        ),
        Wrap(
            spacing: responsivSizeBox('width', 20, context),
            alignment: WrapAlignment.center,
            children: <Widget>[
              card(context, 'Mer', 14, Color(0xff30cfd0), Color(0xff330867),
                  Color(0xff330867)),
              card(context, 'Animaux', 10, Color(0xff30cfd0), Color(0xff330867),
                  Color(0xff330867)),
            ]),
        SizedBox(
          height: responsivSizeBox('height', 20, context),
        ),
        Wrap(
            spacing: responsivSizeBox('width', 10, context),
            children: <Widget>[
              card(context, 'Instruments', 8, Color(0xff30cfd0),
                  Color(0xff330867), Color(0xff330867)),
              card(context, 'Forêt', 13, Color(0xff30cfd0), Color(0xff330867),
                  Color(0xff330867)),
              // card(context, 'Tableau'),
            ]),
      ]),
      decoration: boxDeco(color, Color(0xff84fab0), Color(0xff8fd3f4)));
}

deco(Color color) {
  return BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        Colors.blueAccent,
        Colors.deepPurpleAccent,
        Colors.greenAccent,
      ],
    ),
    boxShadow: ([
      BoxShadow(
        offset: const Offset(1.0, 0),
        blurRadius: 5.0,
        spreadRadius: 1.2,
      ),
    ]),
    border: Border(
      left: BorderSide(
        color: color,
        width: 5,
      ),
      right: BorderSide(
        color: color,
        width: 5,
      ),
      top: BorderSide(
        color: color,
        width: 5,
      ),
      bottom: BorderSide(
        color: color,
        width: 5,
      ),
    ),
    borderRadius: BorderRadius.circular(10),
    color: Colors.lightBlueAccent,
  );
}

card(
    BuildContext context, String s, int i, Color col1, Color col2, Color col3) {
  return Container(
    width: MediaQuery.of(context).size.height / i,
    decoration: BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: [
          col1,
          col2,
          col3,
        ],
      ),
      boxShadow: ([
        BoxShadow(
          offset: const Offset(1.0, 1.0),
          blurRadius: 10.0,
          spreadRadius: 1.2,
        ),
      ]),
      borderRadius: BorderRadius.circular(5),
    ),
    child: Center(
      child: Text(
        s,
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.height / 51,
          color: Colors.white,
        ),
      ),
    ),
  );
}

backChoice(BuildContext context, Color color, Color col1, Color col2) {
  return Container(
      width: IconeChoice('width', context, 'category'),
      height: IconeChoice('height', context, 'category'),
      decoration: boxDeco(color, col1, col2));
}

Container boxAl(BuildContext context, Color color) {
  return Container(
      width: IconeChoice('width', context, 'word'),
      height: IconeChoice('height', context, 'word'),
      child: Column(children: [
        SizedBox(
          height: responsivSizeBox('height', 6, context),
        ),
        Wrap(
            spacing: responsivSizeBox('width', 20, context),
            children: <Widget>[
              card(
                context,
                'Porte',
                14,
                Colors.red,
                Colors.deepOrange,
                Colors.orange,
              ),
              card(
                context,
                'Chien',
                14,
                Colors.red,
                Colors.deepOrange,
                Colors.orange,
              ),
            ]),
        SizedBox(
          height: responsivSizeBox('height', 20, context),
        ),
        Wrap(
            spacing: responsivSizeBox('width', 10, context),
            children: <Widget>[
              card(
                context,
                'Lit',
                18,
                Colors.red,
                Colors.deepOrange,
                Colors.orange,
              ),
              card(context, 'Joie', 18, Colors.red, Colors.deepOrange,
                  Colors.orange),
              card(
                context,
                'Mur',
                18,
                Colors.red,
                Colors.deepOrange,
                Colors.orange,
              ),
            ]),
        SizedBox(
          height: responsivSizeBox('height', 20, context),
        ),
        Wrap(
            spacing: responsivSizeBox('width', 20, context),
            children: <Widget>[
              card(
                context,
                'Coq',
                18,
                Colors.red,
                Colors.deepOrange,
                Colors.orange,
              ),
              card(
                context,
                'Peur',
                18,
                Colors.red,
                Colors.deepOrange,
                Colors.orange,
              ),
              // card(context, 'Tableau'),
            ]),
      ]),
      decoration: boxDeco(color, Colors.blueAccent, Colors.greenAccent));
}

boxDeco(Color color, Color col1, Color col2) {
  return BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        col1,
        col2,
      ],
    ),
    boxShadow: ([
      BoxShadow(
        offset: const Offset(1.0, 1.0),
        blurRadius: 10.0,
        spreadRadius: 1.2,
      ),
    ]),
    border: Border(
      left: BorderSide(
        color: color,
        width: 5,
      ),
      right: BorderSide(
        color: color,
        width: 5,
      ),
      top: BorderSide(
        color: color,
        width: 5,
      ),
      bottom: BorderSide(
        color: color,
        width: 5,
      ),
    ),
    borderRadius: BorderRadius.circular(10),
    color: Colors.lightBlueAccent,
  );
}

Container circle(BuildContext context, String s, w) {
  return Container(
    width: w,
    height: w,
    decoration: BoxDecoration(
      image: new DecorationImage(
        fit: BoxFit.cover,
        image: new AssetImage(
          s,
        ),
      ),
      shape: BoxShape.circle,
    ),
  );
}
