import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jeudessons/Blocs/lupixjeudessons/GameConstruction/GameStarteur.dart';
import 'package:jeudessons/Blocs/lupixjeudessons/Starting/Starting.dart';


class AnimatedLogo extends AnimatedWidget {
  const AnimatedLogo({Key? key, required Animation<double> animation})
      : super(key: key, listenable: animation);

  // Make the Tweens static because they don't change.
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 50, end: 350);

  @override
  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: GestureDetector(
          onTap: () => {
            Future.delayed(
              const Duration(microseconds: 10),
              () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return MyAppStarteur();
                  },
                ),
              ),
            ),
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return Starting();
                },
              ),
            ),
          },
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            height: _sizeTween.evaluate(animation),
            width: _sizeTween.evaluate(animation),
            decoration: BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage('assets/image/bon-timing.png'),
                alignment: Alignment.center,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class LogoApp extends StatefulWidget {
  const LogoApp({Key? key}) : super(key: key);

  @override
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 4), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
