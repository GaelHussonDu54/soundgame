import 'dart:math';

import 'package:jeudessons/Ui/lupixjeudessons/StartingPage/NumberRound.dart';

int randomNum(int value) {
  return Random().nextInt(value);
}

removeValue(List list, var value) {
  while (list.contains(value)) {
    list.remove(value);
  }
}

remplissageCat(List list) {
  if (list.length < NumberRound.choiceRound) {
    list.add(list.elementAt(randomNum(list.length)));
    print('${list.length}<${NumberRound.choiceRound}');
  } else if (NumberRound.choiceRound < list.length) {
    list.removeAt(randomNum(list.length));
    print('${list.length}>${NumberRound.choiceRound}');
  }
}
