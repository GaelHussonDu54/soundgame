import 'package:flutter/cupertino.dart';

double responsivSizeBox(String s, int value, BuildContext context) {
  if (s == 'height') {
    switch (value) {
      case 6:
        {
          return MediaQuery.of(context).size.height / 300;
        }
      case 8:
        {
          return MediaQuery.of(context).size.height / 90;
        }
      case 10:
        {
          return MediaQuery.of(context).size.height / 66;
        }

      case 12:
        {
          return MediaQuery.of(context).size.height / 55;
        }

      case 15:
        {
          return MediaQuery.of(context).size.height / 44;
        }
      case 20:
        {
          return MediaQuery.of(context).size.height / 29;
        }
      case 35:
        {
          return MediaQuery.of(context).size.height / 8;
        }
      default: //25
        {
          return MediaQuery.of(context).size.height / 26;
        }
    }
  } else {
    switch (value) {
      case 10: //20
        {
          return MediaQuery.of(context).size.width / 100;
        }
      case 20: //20
        {
          return MediaQuery.of(context).size.width / 60;
        }
      case 30: //30
        {
          return MediaQuery.of(context).size.width / 40;
        }
      case 60: //30
        {
          return MediaQuery.of(context).size.width / 22;
        }
      default: //50
        {
          return MediaQuery.of(context).size.width / 24;
        }
    }
  }
}

IconeChoice(String s ,BuildContext context, String size){
  if (s == 'height') {
    switch (size) {
      case 'fond':
        {
          return MediaQuery.of(context).size.height * 0.19;
        }

      case 'interact':
        {
          return MediaQuery.of(context).size.height * 0.17;
        }
      case 'category':
        {
          return MediaQuery.of(context).size.height * 0.17;
        }
      case 'word':
        {
          return MediaQuery.of(context).size.height * 0.17;
        }
      case 'order':
        {
          return MediaQuery.of(context).size.height * 0.17;
        }
    }
  } else {
    switch (size) {
      case 'fond':
        {
          return MediaQuery.of(context).size.width * 0.14;
        }
      case 'interact':
        {
          return MediaQuery.of(context).size.width *0.14;
        }
      case 'category':
        {
          return MediaQuery.of(context).size.width *0.14;
        }
      case 'order':
        {
          return MediaQuery.of(context).size.width *0.15;
        }
        case 'word':
      {
        return MediaQuery.of(context).size.width *0.14;
      }

    }
  }
}



containerChoice(String s ,BuildContext context, String size){
  if (s == 'height') {
    switch (size) {
      case 'fond':
        {
          return MediaQuery.of(context).size.height * 0.32;
        }

      case 'interact':
        {
          return MediaQuery.of(context).size.height * 0.32;
        }
      case 'order':
        {
          return MediaQuery.of(context).size.height * 0.32;
        }
      case 'word':
        {
          return MediaQuery.of(context).size.height * 0.32;
        }
      case 'categorie':
        {
          return MediaQuery.of(context).size.height * 0.72;
        }

    }
  } else {
    switch (size) {
      case 'fond':
        {
          return MediaQuery.of(context).size.width * 0.38;
        }
      case 'interact':
        {
          return MediaQuery.of(context).size.width *0.52;
        }
      case 'order':
        {
          return MediaQuery.of(context).size.width *0.52;
        }
      case 'word':
        {
          return MediaQuery.of(context).size.width *0.38;
        }
      case 'categorie':
        {
          return MediaQuery.of(context).size.width *0.76;
        }

    }
  }
}
