import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter_tts/flutter_tts.dart';

AssetsAudioPlayer audioPlayer = AssetsAudioPlayer();
FlutterTts flutterTts = FlutterTts();

List listAllSound=[
  'vague','vent','mouette','pluie','feu',
  'cheval','chien','canard','ane', 'chat', 'oiseaux',
  'bebe','brossagedent','toilette','fermeture', 'aspirateur','machine','sonnette',
  'train','voiture','avion','trainvapeur','bateau','velo','police','pompier',
  'piano','guitare','trompette','tambour', 'acordeon','violon','flute','cloches',

];

List nature=['vague','vent','mouette','pluie','feu'];

List animal=['cheval','chien','canard','ane', 'chat', 'oiseaux' ];

List bruitQuotidien =['bebe','brossagedent','toilette','fermeture', 'aspirateur','machine','sonnette'];

List transport =['train','voiture','avion','trainvapeur','bateau','velo','police','pompier'];
List instrument =['piano','guitare','trompette','tambour', 'acordeon','violon','flute','cloches'];


List getTheList(String s){

  switch (s) {
    case 'Animaux':{
      return animal;
    }
    case 'Nature' :{
      return nature;
    }
    case'Transports':{
      return transport;
    }
    case'Bruits quotidiens':{
      return bruitQuotidien;
    }
    case'Instruments':{
      return instrument;
    }
    default :{
      return animal;

    }


  }
}

String read (String s){
  return 'assets/son/$s.mp3';
}
void listen(String s) {
  audioPlayer.open(Audio('assets/son/$s.mp3'));
}

Future _speak(String s) async {
  await flutterTts.setLanguage('fr-Fr');
  await flutterTts.setPitch(0.35);
  await flutterTts.setVolume(1);
  await flutterTts.speak(s);
}

Future<void> fetchUserOrder(int sec, int mil, String s) {
  // Imagine that this function is fetching user info from another service or database.
  return Future.delayed(
      Duration(seconds: sec, milliseconds: mil), () => _speak(s));
}
choiceRules(int value, int _index, String s, bool order){
  switch (value){
    case 1 : {
      if(order){
        explainRules(_index, s, 'Clique1Ordre');
      }else{
        explainRules(_index, s, 'Clique1');
      }
      }break;
    case 2 : {
      if(order){
        explainRules(_index, s, 'Clique2Ordre');
      }else{
        explainRules(_index, s, 'Clique2');
      }    }break;
    case 3 : {
      if(order){
        explainRules(_index, s, 'GlisserOrdre');
      }else{
        explainRules(_index, s, 'Glisser');
      }
    }break;
  }
}
explainRules(int _index, String s, String rule) {
  if (_index == 0) {
    audioPlayer.open(Audio('assets/$rule'));
  } else if (_index != 0 && _index < 5) {
    fetchUserOrder(2, 600, s);
    Future.delayed(const Duration(seconds: 1, milliseconds: 0),
        () => audioPlayer.open(Audio('assets/TrouveLeMot')));
  }else {
    Future.delayed(const Duration(seconds: 1, milliseconds: 0),
            () => audioPlayer.open(Audio('assets/EndGame')));
  }
}
